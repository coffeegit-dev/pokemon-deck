import { lazy } from 'react';

const routes = [
    {
        path: '/',
        exact: true,
        Component: lazy(() => import('../scenes/WildPokemon'))
    },
    {
        path: '/pokemon-detail/:id',
        exact: true,
        Component: lazy(() => import('../scenes/PokemonDetail'))
    },
    {
        path: '/tamed-pokemons',
        exact: true,
        Component: lazy(() => import('../scenes/TamedPokemon'))
    },
];

export default routes;