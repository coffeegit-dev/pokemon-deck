import { React, Suspense } from 'react';
import {
    Switch,
    Route,
} from 'react-router-dom';
import { 
    Container,
} from 'semantic-ui-react';

import routes from './routes';

// eslint-disable-next-line import/no-anonymous-default-export
export default function() {

    return (
        <div>
        <Container style={{ marginTop: '6em', paddingBottom: '6em' }}>
            <Switch>
                <Suspense fallback={
                    <div>Loading...</div>
                }>
                    {
                        routes.map(({path, Component}, routeIndex) => {
                            return <Route 
                                key={routeIndex} 
                                exact 
                                path={path}
                                component={Component}
                                />
                        })
                    }
                </Suspense>
            </Switch>
        </Container>
        </div>
    );
}