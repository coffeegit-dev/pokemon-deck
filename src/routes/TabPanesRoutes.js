import React from 'react';
import { Tab } from "semantic-ui-react";
import { 
    NavLink,
    Route
} from 'react-router-dom';

import WildPokemon from '../scenes/WildPokemon';
import TamedPokemon from '../scenes/TamedPokemon';

const TabPanesRoutes = [
    {
    menuItem: {
        as: NavLink,
        content: "Wild Pokemon",
        to: "/",
        exact: true,
        key: "wild"
    },
    render: () => (
        <Route path="/" exact>
        {/* <Tab.Pane loading> */}
        <Tab.Pane>
            <WildPokemon />
        </Tab.Pane>
        </Route>
    )
    },
    {
    menuItem: {
        as: NavLink,
        content: "Tamed Pokemon",
        to: "/tamed-pokemon",
        key: "tamed"
    },
    render: () => (
        <Route path="/tamed-pokemon">
        <Tab.Pane>
            {/* <div>Authentication content here</div> */}
            <TamedPokemon />
        </Tab.Pane>
        </Route>
    )
    },
];

export default TabPanesRoutes;