import React from 'react';
import {
    Header,
    Grid,
    Container,
    Card,
    List,
    Image,
    Label,
    Statistic,
    Divider,
    Button
} from 'semantic-ui-react';
import { Link } from "react-router-dom";

const statsItems = [
  { key: 'faves', label: 'Faves', value: '22' },
  { key: 'views', label: 'Views', value: '31,200' },
]

const PokemonDetail = () => {
    return (
        <>
            <Grid verticalAlign='middle'>
                <Grid.Column>
                    <Container fluid textAlign='center'>
                        <Image src='/bulbasaur.png' size='medium' centered/>
                        <div style={{fontSize: '48px', color: '#ffffff', textShadow: '1px 1px #000', marginBottom: '0.5em'}}>Bulbasaur</div>
                        <div style={{marginLeft: 'auto', marginRight: 'auto'}}>
                            <Label circular color='yellow' size='big'>
                                Grass
                            </Label>
                            <Label circular color='orange' size='big'>
                                Poison
                            </Label>
                        </div>
                    </Container>
                    <Card fluid color='pink'>
                        <Card.Content>
                            <Grid columns='two' stackable divided>
                                <Grid.Row>
                                    <Grid.Column textAlign='center'>
                                        <Statistic label='Weight' value='60kg' />
                                    </Grid.Column>
                                    <Grid.Column textAlign='center'>
                                        <Statistic label='Height' value='0.5 m' />
                                    </Grid.Column>
                                </Grid.Row>
                            </Grid>
                            <Divider />
                            <Grid columns='two' stackable divided>
                                <Grid.Row>
                                    <Grid.Column>
                                        <Container>
                                            <Header as='h4'>Stats</Header>
                                            <table width="100%">
                                                <tbody>
                                                    <tr key='stat0'>
                                                        <td width='35%'>HP</td>
                                                        <td>:</td>
                                                        <td>60</td>
                                                    </tr>
                                                    <tr key='stat1'>
                                                        <td>Attack</td>
                                                        <td>:</td>
                                                        <td>62</td>
                                                    </tr>
                                                    <tr key='stat2'>
                                                        <td>Defense</td>
                                                        <td>:</td>
                                                        <td>63</td>
                                                    </tr>
                                                    <tr key='stat3'>
                                                        <td>Special Attack</td>
                                                        <td>:</td>
                                                        <td>80</td>
                                                    </tr>
                                                    <tr key='stat4'>
                                                        <td>Special Defense</td>
                                                        <td>:</td>
                                                        <td>80</td>
                                                    </tr>
                                                    <tr key='stat5'>
                                                        <td>Speed</td>
                                                        <td>:</td>
                                                        <td>60</td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </Container>
                                    </Grid.Column>
                                    <Grid.Column>
                                        <Container>
                                            <Header as='h4'>Abilities</Header>
                                            <List items={['Overgrowl', 'Chlorophyl', 'Lightning Bulb']} />
                                        </Container>
                                    </Grid.Column>
                                </Grid.Row>
                            </Grid>
                        </Card.Content>
                        <Card.Content extra textAlign='center'>
                            <Link to='/'>
                                <Button color='blue'>
                                    Catch Pokemon
                                </Button>
                            </Link>
                        </Card.Content>
                    </Card>
                </Grid.Column>
            </Grid>
        </>
    ); 
};

export default PokemonDetail;