import React from 'react';
import {
    Grid,
} from 'semantic-ui-react';
import { useLocation } from "react-router-dom";

import PokemonCard from '../components/PokemonCard';

const WildPokemon = () => {
    let location = useLocation();

    const showOwnedCount = ({ location }) => {
        return location.pathname !== '/' ? true : false;
    }

    const isOwnedPokemon = () => {
        return true;
    }

    const pokemons = ['Bulbasaur', 'Pikachu', 'Duoduo', 'Ivysaur', 'Charmeleon', 'Wartortle', 'Butterfree', 'Meow', 'Kakuna'];

    return (
    <>
        {/* <Grid columns={4} stackable> */}
        <Grid columns={4} doubling>
        {
            pokemons.length ? 
                pokemons.map((pokemon, index) => {
                    console.log('index:' + index);
                    return (
                        <PokemonCard index={index} name={pokemon} sprites={pokemons} showOwnedCount='true' ownedCount='5' detailUrl='/pokemon-detail/:id' />
                    )
                }) : ('')
        }
        </Grid>
    </>
    );
};

export default WildPokemon;