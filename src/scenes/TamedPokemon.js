import React from 'react';
import {
    Grid,
} from 'semantic-ui-react';
import { useLocation } from "react-router-dom";

import OwnedPokemonCard from '../components/OwnedPokemonCard';

const TamedPokemon = () => {
    let location = useLocation();

    const pokemons = ['Bulbasaur', 'Pikachu', 'Duoduo', 'Ivysaur', 'Charmeleon', 'Wartortle', 'Butterfree', 'Meow', 'Kakuna'];

    return (
    <>
        <Grid columns={4} doubling>
            {
                pokemons.length ? 
                    pokemons.map((pokemon, index) => {
                        console.log('index:' + index);
                        return (
                            <OwnedPokemonCard index={index} name={pokemon} sprites={pokemons} detailUrl='/pokemon-detail/:id' />
                        )
                    }) : ('')
            }
        </Grid>
    </>
    );
};

export default TamedPokemon;