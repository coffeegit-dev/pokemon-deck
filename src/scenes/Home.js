import React from 'react';
import { withRouter } from "react-router-dom";

import Routes from '../routes/index';
import TopBar from '../components/TopBar';
import FooterMenu from '../components/FooterMenu';

// const Home = ({location}) => (
const Home = () => (
    <div>
        <TopBar />
        <Routes />
        <FooterMenu />
    </div>
)

export default withRouter(Home);
// export default Home;