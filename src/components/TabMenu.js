import React from 'react';
import { 
    Container, 
    Tab ,
} from "semantic-ui-react";

import TabPanesRoutes from '../routes/TabPanesRoutes';

const TabMenu = () => (
    <Container fluid style={{ marginTop: '5em' }}>
        <Tab menu={{ secondary: true, pointing: true }} panes={TabPanesRoutes} />
    </Container>
);

export default TabMenu;