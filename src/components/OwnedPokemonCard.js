import React, { useState } from 'react';
import {
    Grid,
    Card,
    Image,
    Label,
    Button,
} from 'semantic-ui-react';
import { Link } from "react-router-dom";

const OwnedPokemonCard = (props) => {
    const {
        index,
        sprites,
        name,
        detailUrl,
    } = props;

    return(
        <>
        <Grid.Column>
            <Link key={index} to={detailUrl}>
                <Card fluid>
                    <Card.Content>
                        <Image src='/bulbasaur.png' wrapped ui={true} />
                        <Card.Header textAlign="center">Bulbasaur</Card.Header>
                        <Card.Meta textAlign="center">Mbulbasaur</Card.Meta>
                        <Card.Description textAlign='center'>
                            <Link to='/'>
                                <Button color='red'>
                                    Release
                                </Button>
                            </Link>
                        </Card.Description>
                    </Card.Content>
                </Card>
            </Link>
        </Grid.Column>
        </>
    );
};

export default OwnedPokemonCard;