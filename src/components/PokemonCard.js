import React, { useState } from 'react';
import {
    Grid,
    Card,
    Image,
    Label,
} from 'semantic-ui-react';
import { Link } from "react-router-dom";

const PokemonCard = (props) => {
    const {
        index,
        sprites,
        name,
        showOwnedCount,
        ownedCount,
        detailUrl,
    } = props;

    const [avatar, setAvatar] = useState('');

    const showFrontAvatar = _ =>{
        if (sprites) {
            setAvatar(sprites.front_default);
        }
    }

    const showBackAvatar = _ =>{
        if (sprites) {
            setAvatar(sprites.back_default);
        }
    }

    useState(() => {
        if (sprites) {
            setAvatar(sprites.front_default);
        }
    }, []);

    let isMobileSize = (window.innerWidth <= 760);

    return(
        <>
        <Grid.Column>
            <Link key={index} to={detailUrl}>
            <Card fluid>
                <Card.Content>
                    <Image src='/bulbasaur.png' wrapped ui={true} />
                    <Card.Header textAlign="center" style={{ marginTop: '0.5em' }}>{name}</Card.Header>
                    {
                        showOwnedCount && 
                        <Card.Description>
                            <Label color='orange' ribbon>
                                owned : {ownedCount}
                            </Label>
                        </Card.Description>
                    }
                    
                </Card.Content>
            </Card>
            </Link>
        </Grid.Column>
        </>
    );
};

export default PokemonCard;