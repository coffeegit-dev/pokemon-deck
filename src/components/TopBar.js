import React from 'react';
import { 
    Container,
    Menu, 
    Grid,
    Image,
} from 'semantic-ui-react';

const TopBar = _ => (
    <div>
        <Menu fixed='top' inverted color="pink">
            <Container>
                <Grid columns={1} style={{ marginLeft: 'auto', marginRight: 'auto', textColor: '#FFFFFF' }}>
                    <Grid.Row>
                    <Grid.Column>
                        <Image size='small' src='/poke_ball_icon.svg' style={{ width: '64px', height: '64px'}} />
                    </Grid.Column>
                    </Grid.Row>
                </Grid>
            </Container>
        </Menu>
    </div>
)

export default TopBar;