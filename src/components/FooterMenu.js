import React, { useState } from 'react';
import {
    Icon,
    Menu,
} from 'semantic-ui-react';
import {
    NavLink
} from 'react-router-dom'

const FooterMenu = _ => {
    const [activeItem, setActiveItem] = useState({});
    
    let handleItemClick = (e, { name }) => {
        setActiveItem({ activeItem: name })
    };

    return (
        <>
            <Menu fluid fixed="bottom" widths={2} icon="labeled" inverted>
                <Menu.Item
                    as={NavLink} to="/"
                    name='wild'
                    active={activeItem === 'wild'}
                    onClick={handleItemClick}
                >
                    <Icon name='list' />
                    Wild Pokemon
                </Menu.Item>
                <Menu.Item
                    as={NavLink} to="/tamed-pokemons"
                    name='tamed'
                    active={activeItem === 'tamed'}
                    onClick={handleItemClick}
                >
                    <Icon name='list ul' />
                    Tamed Pokemon
                </Menu.Item>
            </Menu>
        </>
    );
};

export default FooterMenu;